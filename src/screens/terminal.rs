extern crate crossterm;
use crate::FlatResult as Result;
use crate::screens::Renderer;
use crate::eventhandlers::terminal::Handler;
use crossterm::{Screen as Terminal, Crossterm};
use crossterm::{ClearType, Color};
use std::io::{Read, Write};

pub struct Screen{
    width: usize,
    height: usize,
    screen: Terminal,
    prev_screen: Vec<[u8;8]>
}


impl Renderer for Screen{
    type Backend = Screen;
    type EventHandler = Handler;

    fn create(width: usize, height: usize) -> Result<(Screen, Handler)>{
    let screen = Terminal::new(true);
    let crossterm = Crossterm::from_screen(&screen);
    let cursor = crossterm.cursor();

    let input = crossterm.input();
    let events = Handler::new(input.read_async().bytes());
    crossterm.terminal().clear(ClearType::All).unwrap();
    cursor.hide().unwrap();
    // Create the initial (black) screen...
    let mut prev_screen = vec![];
    for _ in 0..(width * (height / 2)){
        prev_screen.push([0,0,0,0, 0,0,0,0])
    };
    
    Ok((Screen{
        width,
        height,
        screen,
        prev_screen,
    }, events))
    }

    fn draw(&mut self, data: &[u8]) -> Result<()>{
        let crossterm = Crossterm::from_screen(&self.screen);
        let cursor = crossterm.cursor();
        let gap = self.width * 4;
        for row in (0..self.height).step_by(2){
            for pix in (0..self.width*4).step_by(4){
                let coord = (row / 2)*(self.width) + (pix/4);
                let prev = &self.prev_screen[coord];
                let start = pix + (row * self.width * 4);
                let new = [
                   data[start+2],
                   data[start+1],
                   data[start],
                   255,
                   data[start + 2 + gap],
                   data[start + 1 + gap],
                   data[start + gap],
                   255
                ];
                if prev != &new {
                    self.prev_screen[coord] = new;
                    cursor.goto((pix/4) as u16, (row/2) as u16).unwrap();
                    crossterm.style("▀")
                       .with(Color::Rgb{
                           r: new[0],
                           g: new[1],
                           b: new[2]
                       }).on(Color::Rgb{
                           r: new[4],
                           g: new[5],
                           b: new[6],
                       }).paint(&self.screen.stdout).unwrap();
                };
               self.screen.flush().unwrap();
            }
        }
        Ok(())
    }

    fn resize(&mut self, width: f64, height: f64) -> Result<()>{
        Ok(())
    }

    fn refresh(&mut self){}
}


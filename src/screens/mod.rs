use crate::FlatResult as Result;

pub trait Renderer {
    type Backend;
    type EventHandler;

    fn create(width: usize, height:usize) -> Result<(Self::Backend, Self::EventHandler)>;

    /// Draws texture to the screen, refreshing directly after
    fn draw(&mut self, data: &[u8]) -> Result<()>;
    /// Resizes the screen. Only works for Window Backend
    fn resize(&mut self, width: f64, height: f64) -> Result<()>;
    /// Refreshes the current screen WITHOUT altering contents
    fn refresh(&mut self);
}

pub mod window;
pub mod terminal;
pub mod framebuf;

use crate::eventhandlers::{Event, EventHandler, Key};
use glutin::{WindowEvent, VirtualKeyCode, ElementState};
use glutin::Event as GEvent;
use glutin::EventsLoop;

impl From<Option<VirtualKeyCode>> for Key{
    fn from(item: Option<VirtualKeyCode>) -> Self{
        match item {
            Some(x) => match x {
                VirtualKeyCode::A => Key::A,
                VirtualKeyCode::B => Key::B,
                VirtualKeyCode::C => Key::C,
                VirtualKeyCode::D => Key::D,
                VirtualKeyCode::E => Key::E,
                VirtualKeyCode::F => Key::F,
                VirtualKeyCode::G => Key::G,
                VirtualKeyCode::H => Key::H,
                VirtualKeyCode::I => Key::I,
                VirtualKeyCode::J => Key::J,
                VirtualKeyCode::K => Key::K,
                VirtualKeyCode::L => Key::L,
                VirtualKeyCode::M => Key::M,
                VirtualKeyCode::N => Key::N,
                VirtualKeyCode::O => Key::O,
                VirtualKeyCode::P => Key::P,
                VirtualKeyCode::Q => Key::Q,
                VirtualKeyCode::S => Key::S,
                VirtualKeyCode::T => Key::T,
                VirtualKeyCode::U => Key::U,
                VirtualKeyCode::V => Key::V,
                VirtualKeyCode::W => Key::W,
                VirtualKeyCode::X => Key::X,
                VirtualKeyCode::Y => Key::Y,
                VirtualKeyCode::Z => Key::Z,
                VirtualKeyCode::Space => Key::SPACE,
                _ => Key::NONE,
            },
            None => Key::NONE
        }
    }
}

impl From<WindowEvent> for Event{
    fn from(item: WindowEvent) -> Self {
        match item {
            WindowEvent::Resized(x) => Event::Resize(x.width, x.height),
            WindowEvent::CloseRequested => Event::Close,
            WindowEvent::KeyboardInput{input, ..} => match (input.state, input.virtual_keycode) {
                 (ElementState::Pressed, v) => Event::KeyPress(v.into()),
                 (ElementState::Released, v) => Event::KeyRelease(v.into()),
            },
            _ => Event::Ignored
        }
    }
}

pub struct Handler{
    event_loop: EventsLoop
}

impl Handler{
    pub fn new(event_loop: EventsLoop)->Handler{
        Handler{event_loop}
    }
}

impl EventHandler for Handler{
    fn catch<F: FnMut(Event)>(&mut self, mut callback: F){
        self.event_loop.poll_events(|e| match e {
            GEvent::WindowEvent{event, ..} => callback(event.into()),
            _ => ()
        });
    }
}

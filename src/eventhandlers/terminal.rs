use crate::eventhandlers::{Event, EventHandler, Key};
use crossterm::{AsyncReader};
use std::io::{Bytes};

pub struct Handler {
    stdin: Bytes<AsyncReader>
}


impl From<u8> for Event{
    fn from(item: u8) -> Self {
        match item {
          b'a' => Event::KeyPress(Key::A),
          b'b' => Event::KeyPress(Key::B),
          b'c' => Event::KeyPress(Key::C),
          b'd' => Event::KeyPress(Key::D),
          b'e' => Event::KeyPress(Key::E),
          b'f' => Event::KeyPress(Key::F),
          b'g' => Event::KeyPress(Key::G),
          b'h' => Event::KeyPress(Key::H),
          b'i' => Event::KeyPress(Key::I),
          b'j' => Event::KeyPress(Key::J),
          b'k' => Event::KeyPress(Key::K),
          b'l' => Event::KeyPress(Key::L),
          b'm' => Event::KeyPress(Key::M),
          b'n' => Event::KeyPress(Key::N),
          b'o' => Event::KeyPress(Key::O),
          b'p' => Event::KeyPress(Key::P),
          b'q' => Event::KeyPress(Key::Q),
          b'r' => Event::KeyPress(Key::R),
          b's' => Event::KeyPress(Key::S),
          b't' => Event::KeyPress(Key::T),
          b'u' => Event::KeyPress(Key::U),
          b'v' => Event::KeyPress(Key::V),
          b'w' => Event::KeyPress(Key::W),
          b'x' => Event::KeyPress(Key::X),
          b'y' => Event::KeyPress(Key::Y),
          b'z' => Event::KeyPress(Key::Z),
          b' ' => Event::KeyPress(Key::SPACE),
          3 => Event::Close,
          _ => Event::KeyPress(Key::NONE),
        }
    }
}


impl Handler {
    pub fn new(stdin: Bytes<AsyncReader>) -> Handler{
        return Handler{stdin}
    }
}

impl EventHandler for Handler{
    fn catch<F: FnMut(Event)>(&mut self, mut callback: F){
        // Read until we hit a none...
        let mut reading = true;
        while reading {
            match self.stdin.next(){
                Some(Ok(x)) => callback(x.into()),
                _ => reading = false,
            }
        }
    }
        
}

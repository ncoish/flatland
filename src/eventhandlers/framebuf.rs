extern crate minifb;
use crate::eventhandlers::{Event, EventHandler, Key};
use minifb::Key as VirtualKeyCode;
use minifb::{Window, KeyRepeat};

impl From<Option<VirtualKeyCode>> for Key{
    fn from(item: Option<VirtualKeyCode>) -> Self{
        match item {
            Some(x) => match x {
                VirtualKeyCode::A => Key::A,
                VirtualKeyCode::B => Key::B,
                VirtualKeyCode::C => Key::C,
                VirtualKeyCode::D => Key::D,
                VirtualKeyCode::E => Key::E,
                VirtualKeyCode::F => Key::F,
                VirtualKeyCode::G => Key::G,
                VirtualKeyCode::H => Key::H,
                VirtualKeyCode::I => Key::I,
                VirtualKeyCode::J => Key::J,
                VirtualKeyCode::K => Key::K,
                VirtualKeyCode::L => Key::L,
                VirtualKeyCode::M => Key::M,
                VirtualKeyCode::N => Key::N,
                VirtualKeyCode::O => Key::O,
                VirtualKeyCode::P => Key::P,
                VirtualKeyCode::Q => Key::Q,
                VirtualKeyCode::S => Key::S,
                VirtualKeyCode::T => Key::T,
                VirtualKeyCode::U => Key::U,
                VirtualKeyCode::V => Key::V,
                VirtualKeyCode::W => Key::W,
                VirtualKeyCode::X => Key::X,
                VirtualKeyCode::Y => Key::Y,
                VirtualKeyCode::Z => Key::Z,
                VirtualKeyCode::Space => Key::SPACE,
                _ => Key::NONE,
            },
            None => Key::NONE
        }
    }
}


pub struct Handler<'a>{
    window: &'a Window,
    pressed_keys: Vec<VirtualKeyCode>,
}

impl<'a> Handler<'a>{
    pub fn new(window: &'a Window)->Handler{
        Handler{
            window,
            pressed_keys: vec!()
        }
    }
}

impl<'a> EventHandler for Handler<'a>{
    fn catch<F: FnMut(Event)>(&mut self, mut callback: F){
        // Grab any keys released
        let mut released_keys = vec![];
        for (i, key) in self.pressed_keys.iter().enumerate(){
            if self.window.is_key_released(*key){
                println!("released {:?}", key);
                released_keys.push(i);
            }
        };
        released_keys.reverse();
        // remove 'em from our pressed keys...
        for i in released_keys.iter(){
            self.pressed_keys.remove(*i);
        }

        match self.window.get_keys_pressed(KeyRepeat::No){
            Some(keys) => {
                println!("Pressing {:?}", keys);
                self.pressed_keys.extend_from_slice(keys.as_slice());
            },
            _=>()
        };
    }
}

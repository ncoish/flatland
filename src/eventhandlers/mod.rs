#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Key{
    NONE,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    SPACE,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Event{
    Close,
    Resize(f64,f64),
    KeyPress(Key),
    KeyRelease(Key),
    Ignored,
}


pub trait EventHandler{
    fn catch<F: FnMut(Event)>(&mut self, callback: F);
}


pub mod terminal;
pub mod window;
pub mod framebuf;

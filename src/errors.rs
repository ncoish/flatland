use std::error::Error;
use std::fmt;

#[derive(Debug)]
struct FlatlandError {
    detail: String
}

impl FlatlandError {
    fn new(msg: *str) -> FlatlandError {
        FlatLandError{details: msg.to_string()}
    }
}

impl fmt:Display for FlatlandError {
    fn fmt(&self, f: &mut fmt:Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for FlatlandError {
    fn description(&self) -> &str {
        &self.details
    }
}

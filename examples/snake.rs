use flatland::screens::{Renderer, window::Screen};
use flatland::eventhandlers::{EventHandler, Event, Key};
use flatland::canvas::Canvas;
use flatland::pixel::{RGB, Pixel};
use std::path::Path;

extern crate image;
extern crate rand;
use rand::{thread_rng, Rng};

const WIDTH: usize = 100;
const HEIGHT: usize = 100;

#[derive(Clone, Copy, PartialEq, Eq)]
enum GameState{
    NoState,
    Intro,
    // Never ended up using level integer
    Level(u8),
    Death,
}

enum Direction{
    Up, Down, Left, Right
}

fn rand_coords() -> [usize;2]{
    let mut rng = thread_rng();
    [rng.gen_range(0, WIDTH), rng.gen_range(0, HEIGHT)]
}

fn main(){
    // Create Screen / EventHandler
    let (mut screen, mut events) = Screen::create(WIDTH, HEIGHT).unwrap();

    // Load Images
    let intro = image::open(&Path::new("examples/resources/snake-intro.png"))
        .unwrap().to_bgra().into_raw();
    let death = image::open(&Path::new("examples/resources/snake-death.png"))
        .unwrap().to_bgra().into_raw();
    let mut canvas = Canvas::new(WIDTH, HEIGHT);
    
    let mut running = true;
    let mut prevstate = GameState::NoState;
    let mut state = GameState::Intro;
    let mut going_to_die = false;
    let mut snake_direction = Direction::Down;
    let mut snake: Vec<[usize; 2]> = vec![];
    let mut max_length = 4;
    let mut snake_position: [i8; 2] = [10, 10];
    let mut food_position = [50, 50];
    let WHITE: [u8;4] = RGB::new(255, 255, 255).raw();

    while running {
        // Handle events (close is pretty mandatory)
        events.catch(|event| {
            match event{
                Event::Close => running = false,
                Event::Resize(x,y) => {
                    screen.resize(x, y).unwrap();
                    screen.refresh();
                },
                e => {
                    match state {
                        GameState::Intro => {
                            match e {
                                Event::KeyPress(Key::SPACE) => {
                                    state = GameState::Level(1);
                                }
                                _ => ()
                            }
                        },
                        GameState::Death => {
                            match e {
                                Event::KeyPress(Key::SPACE) => {
                                    going_to_die = false;
                                    snake = vec![];
                                    max_length = 4;
                                    state = GameState::Intro;
                                }
                                _ => ()
                            }
                        },
                        GameState::Level(_) => {
                            match e {
                                Event::KeyPress(Key::H) => snake_direction = Direction::Left,
                                Event::KeyPress(Key::J) => snake_direction = Direction::Down,
                                Event::KeyPress(Key::K) => snake_direction = Direction::Up,
                                Event::KeyPress(Key::L) => snake_direction = Direction::Right,
                                _ => ()
                            }
                        }
                        _ => ()
                    }
                }
            }
        });

        // Always copy that prevstate

        match state {
            GameState::Intro => {
                if state != prevstate{
                    screen.draw(intro.as_slice()).unwrap()
                }
            },
            GameState::Death => {
                if state != prevstate{
                    screen.draw(death.as_slice()).unwrap()
                }
            },
            GameState::Level(_) => {
               //Put the snake in there
               snake.push([snake_position[0] as usize, snake_position[1] as usize]);
               if snake.len() > max_length{
                   snake.rotate_left(1);
                   snake.pop();
               }

               // Update snake's location
               match snake_direction {
                   Direction::Up => snake_position[1] = snake_position[1] - 1,
                   Direction::Down => snake_position[1] = snake_position[1] + 1,
                   Direction::Left => snake_position[0] = snake_position[0] - 1,
                   Direction::Right => snake_position[0] = snake_position[0] + 1,
               }

               if snake_position[0] < 0 {
                   snake_position[0] = 99;
               }
               if snake_position[1] < 0 {
                   snake_position[1] = 99;
               }
               if snake_position[0] > 99 {
                   snake_position[0] = 0;
               }
               if snake_position[1] > 99 {
                   snake_position[1] = 0;
               }

               // For simplicity's sake, we'll make a usized snake position
               let spos = [snake_position[0] as usize, snake_position[1] as usize];
               // Draw the snake
               canvas.clearall();
               for p in &snake{
                   canvas.set_pixel(p[0], p[1], &WHITE).unwrap();
               }
               let collision = canvas.get_pixel(spos[0], spos[1]);
               let mut got_food = false;
               if spos == food_position{
                   got_food = true;
               }
               if collision[0] == 255 && !got_food {
                   going_to_die = true;
               } 
               else if got_food {
                   max_length += 4;
                   // Get new food coords
                   let mut found_empty = false;
                   while !found_empty{
                       food_position = rand_coords();
                       if canvas.get_pixel(food_position[0], food_position[1])[0] != 255{
                           found_empty = true;
                       }

                   }
               }

               // Draw the head and the food
               canvas.set_pixel(food_position[0], food_position[1], &WHITE).unwrap();
               canvas.set_pixel(spos[0], spos[1], &WHITE).unwrap();
               screen.draw(canvas.as_slice()).unwrap();
               
            },
            _ => ()
        }
        prevstate = state.clone();
        match going_to_die {
            true => state = GameState::Death,
            false => ()
        }
    }
}

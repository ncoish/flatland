use flatland::screens::{Renderer, terminal::Screen};
use flatland::eventhandlers::{EventHandler, Event, Key};
use std::path::Path;

extern crate image;
use image::GenericImage;

const WIDTH: usize = 32;
const HEIGHT: usize = 32;

fn main(){
    // Create Screen / EventHandler
    let (mut screen, mut events) = Screen::create(WIDTH, HEIGHT).unwrap();

    // Load Image & Draw to screen
    let img = image::open(&Path::new("examples/resources/image.png")).unwrap().to_bgra();
    screen.draw(img.into_raw().as_slice());

    let mut running = true;
    while running {
        // Handle events (close is pretty mandatory)
        events.catch(|event| {
            match event{
                Event::Close => running = false,
                Event::Resize(x,y) => {
                    screen.resize(x, y).unwrap();
                    screen.refresh();
                },
                _ => ()
            }
        });
    }
}

